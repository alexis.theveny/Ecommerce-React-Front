import '../App.css';
import React, { useContext, useEffect, useState } from 'react';
import Card from '../components/card';
import { getAllProducts, getProduct } from '../api/product';
import { useParams } from 'react-router-dom';
import { EcommerceContext } from '../context/ecommerce';


function ProductView() {
    const [cards, setCards] = useState([])
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getAllProducts().then(data => {
            setCards(data)
        })
    }, [])
    return (
        <div className='app'>
            <div className='product-list'>
                {cards.map(product => (
                    <Card key={product.id} id={product.id} nom={product.nom} description={product.description} prix={product.prix} photo={product.photo}></Card>
                ))}
            </div>
        </div>
    );
}
function ProductDetailView() {
    const { id } = useParams();
    const { cartProducts, setCartProducts } = useContext(EcommerceContext);
    const [product, setProducts] = useState({})
    const [quantity, setQuantity] = useState(1)
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getProduct(id).then(data => {
            setProducts(data)
        })
    }, [])
    const submitForm = (e) => {
        e.preventDefault();
        console.log(cartProducts);
        // eslint-disable-next-line
        const index = cartProducts.findIndex(elem => elem.product.id == product.id)
        if(index === -1){
            console.log(index);
            console.log('ajout produit au panier');
            setCartProducts([...cartProducts, { product, quantity: (+quantity) }])
        } else {
            console.log('modif produit au panier');
            cartProducts[index].quantity += (+quantity)
            setCartProducts( [...cartProducts])
        }
        console.log(product, quantity)
    }
    return (
        <div className='app'>
            <div className='product-detail-list'>
                <div>
                    <div className='product-img' >
                        <img src={product.photo} alt='produit'/>
                    </div>
                    <div className='product-detail'>
                        <div className='product-info'>
                            <p className="title is-4" title={product.nom} style={{ color: '#00D1B2' }}>{product.nom}</p>
                            <br />
                            <p className="subtitle is-6 box button is-primary is-light" style={{ fontWeight: 'bold' }}>{product.prix} €</p>
                            <br />
                            <div>
                                {product.description}
                            </div>
                            <br />
                        </div>
                        <form className='product-buy' action="/" method='post' onSubmit={submitForm}>
                            <p className='title is-5'>Quantité : </p>
                            <input className="input" type="number" value={quantity} onChange={(e) => setQuantity(e.target.value <= 0 ? 1 : e.target.value)} />
                            <br /><br />
                            <button className="button is-primary" style={{ width: '100%' }}>Ajouter au panier</button>
                        </form>
                    </div>
                </div>
                <div>
                    Suggestion de Produits <br />
                </div>
            </div>
        </div>
    );
}
export { ProductView, ProductDetailView };