import '../../App.css';
import React, { useContext, useEffect, useState } from 'react';
import { getAllProducts, getProduct } from '../../api/product'
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Pencil, TrashFill } from 'react-bootstrap-icons'
import {addProduct, editProduct, deleteProduct} from '../../api/settings/product'
import { EcommerceContext } from '../../context/ecommerce';


function ProductSettingView() {
    const [products, setProducts] = useState([])
    const [keys, setKeys] = useState([])
    
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getAllProducts().then(data => {
            setProducts(data)
            setKeys(Object.keys(data[0]))
        })
    }, [])
    return (
        <div className='app'>
            <h1 className='title is-4'>Paramètres - Produits</h1>
            <Link to={'/settings/product/add'} className="button is-primary">Ajouter un produit</Link>
            <table className="table">
                <thead>
                    <tr>
                        {keys.map(key => (
                            <th><abbr title={key}>{key}</abbr></th>
                        ))}
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product => (
                        <tr>
                            {keys.map(key => (
                                <td>{product[key]}</td>
                            ))}
                            <td className="is-flex is-flex-direction-column">
                                <Link to={`/settings/product/edit/${product['id']}`} className="button is-info my-1"><Pencil /></Link>
                                <Link to={`/settings/product/delete/${product['id']}`} className="button is-danger my-1"><TrashFill /></Link>
                            </td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div >
    );
}
function ProductAddView() {
    const { token } = useContext(EcommerceContext);

    const [nom, setNom] = useState("")
    const [description, setDescription] = useState("")
    const [prix, setPrix] = useState(0)
    const [photo, setPhoto] = useState("")
    const navigate = useNavigate()

    const submitForm = (e) => {
        console.log(nom, description, prix, photo, token);
        //todo
        e.preventDefault();
        addProduct(nom, description, prix, photo, token).then(data => {
            alert(data.mess)
            navigate('/settings/product')
        })
    }

    return (
        <div className='app'>
            <h1 className='title is-4'>Paramètres - Produits - Ajout</h1>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Ajout</h1>
                    <input className="input" type="text" placeholder="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                    <input className="input" type="text" placeholder="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                    <input className="input" type="number" placeholder="prix" step={0.01} value={prix} onChange={(e) => setPrix(e.target.value <= 0 ? 0 : e.target.value)} />
                    <input className="input" type="text" placeholder="photo" value={photo} onChange={(e) => setPhoto(e.target.value)} />
                    <button className="button is-primary is-light">Créer le produit</button>
                </form>
            </div>

        </div >
    );
}
function ProductEditView() {
    const { token } = useContext(EcommerceContext);

    const { id } = useParams();
    const [nom, setNom] = useState("")
    const [description, setDescription] = useState("")
    const [prix, setPrix] = useState(0)
    const [photo, setPhoto] = useState("")
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getProduct(id).then(data => {
            setNom(data.nom)
            setDescription(data.description)
            setPrix(data.prix)
            setPhoto(data.photo)
        })
    }, [])


    const navigate = useNavigate()

    const submitForm = (e) => {
        e.preventDefault();
        editProduct(id, nom, description, prix, photo, token).then(data => {
            alert(data.mess)
            navigate('/settings/product')
        })
    }
    return (
        <div className='app'>
            <h1 className='title is-4'>Paramètres - Produits - Edit</h1>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Edit</h1>
                    <input className="input" type="text" placeholder="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                    <input className="input" type="text" placeholder="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                    <input className="input" type="number" placeholder="prix" step={0.01} value={prix} onChange={(e) => setPrix(e.target.value <= 0 ? 0 : e.target.value)} />
                    <input className="input" type="text" placeholder="photo" value={photo} onChange={(e) => setPhoto(e.target.value)} />
                    <button className="button is-primary is-light">Editer le produit</button>
                </form>
            </div>

        </div >
    );
}
function ProductDeleteView() {
    const { token } = useContext(EcommerceContext);

    const navigate = useNavigate()
    const { id } = useParams();

    useEffect(() => {
        deleteProduct(id, token).then(data => {
            alert(data.mess)
            navigate('/settings/product')
        })
    }, [])
}
export { ProductSettingView, ProductAddView, ProductEditView, ProductDeleteView };