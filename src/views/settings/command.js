import '../../App.css';
import React, { useContext, useEffect, useState } from 'react';
import { editCommand, deleteCommand } from '../../api/settings/command';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Pencil, TrashFill } from 'react-bootstrap-icons'
import { getAllCommands, getCommand } from '../../api/command';
import { EcommerceContext } from '../../context/ecommerce';



function CommandSettingView() {
    const { token } = useContext(EcommerceContext);

    const [commands, setCommands] = useState([])
    const [keys, setKeys] = useState([])

    useEffect(() => {
        // write your code here, it's like componentWillMount
        getAllCommands(token).then(data => {
            setCommands(data)
            setKeys(Object.keys(data[0]))
        })
    }, [])
    return (
        <div className='app'>
            <h1 className='title is-4'>Paramètres - Commandes</h1>
            <table className="table">
                <thead>
                    <tr>
                        {keys.map(key => (
                            <th><abbr title={key}>{key}</abbr></th>
                        ))}
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {commands.map(command => (
                        <tr>
                            {keys.map(key => (
                                <td>{command[key]}</td>
                            ))}
                            <td className="is-flex is-flex-direction-column">
                                <Link to={`/settings/command/edit/${command['id']}`} className="button is-danger my-1"><Pencil /></Link>
                                <Link to={`/settings/command/delete/${command['id']}`} className="button is-danger my-1"><TrashFill /></Link>
                            </td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}
function CommandEditView() {
    const { token } = useContext(EcommerceContext);

    const { id } = useParams();
    const [statut, setStatut] = useState("")
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getCommand(id).then(data => {
            setStatut(data.statut)
        })
    }, [])


    const navigate = useNavigate()
    const statusChanged = (e)=> {
        setStatut(e.target.value)
    }

    const submitForm = (e) => {
        e.preventDefault();
        editCommand(id, statut, token).then(data => {
            alert(data.mess)
            navigate('/settings/command')
        })
    }
    return (
        <div className='app'>
            <h1 className='title is-4'>Paramètres - Commande - Edit</h1>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Edit</h1>
                    <select value={statut} onChange={statusChanged}>
                        <option value="Validé">Validé</option>
                        <option value="Payé">Payé</option>
                        <option value="Livré">Livré</option>
                        <option value="Annulé">Annulé</option>
                    </select>
                    <button className="button is-primary is-light">Editer le statut de la commande</button>
                </form>
            </div>

        </div >
    );
}
function CommandDeleteView() {
    const { token } = useContext(EcommerceContext);

    const navigate = useNavigate()
    const { id } = useParams();

    useEffect(() => {
        deleteCommand(id, token).then(data => {
            alert(data.mess)
            navigate('/settings/command')
        })
    }, [])
}

export {CommandSettingView, CommandEditView, CommandDeleteView};