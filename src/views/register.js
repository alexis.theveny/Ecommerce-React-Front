import '../App.css';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { registerUser } from '../api/user';


function registerView() {
    const [email, setEmail] = useState("")
    const [pass, setPass] = useState("")
    const [confirm, setConfirm] = useState("")
    const navigate = useNavigate()


    const submitForm = (e) => {
        e.preventDefault();
        if (pass !== confirm) {
            alert("Les mots de passes sont différents")
        }
        else {
            registerUser(email,pass).then(data => {
            if(data.id !== undefined){
                alert(data.mess)
                navigate("/login")
            }else{
                alert(data)
            }
        })

            console.log(email, pass, confirm);
        }
    }

    return (
        <div className='app'>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Inscription</h1>
                    <input className="input" type="email" placeholder="Mail" value={email} onChange={(e) => setEmail(e.target.value)} />
                    <input className="input" type="password" placeholder="Password" value={pass} onChange={(e) => setPass(e.target.value)} />
                    <input className="input" type="password" placeholder="Confirm Password" value={confirm} onChange={(e) => setConfirm(e.target.value)} />
                    <button className="button is-primary is-light">Inscription</button>
                </form>
            </div>
        </div>
    );
}
export default registerView;