import '../App.css';
import React, { useContext, useEffect, useState } from 'react';
import 'bulma/css/bulma.min.css'
import { EcommerceContext } from '../context/ecommerce';
import { getAllCommands } from '../api/command';
import { Eye } from 'react-bootstrap-icons'
import { Link } from 'react-router-dom';


function HomeView() {
  const { token } = useContext(EcommerceContext);
  const [commands, setCommands] = useState([])
  const [keys, setKeys] = useState([])
  useEffect(() => {
    if (token && token !== '') {
      getAllCommands(token).then(data => {
        console.log(data);
        setCommands(data);
        setKeys(Object.keys(data[0]));
      })
    }
  }, [])

  return (
    <div className='app'>
      <h1 className='title is-2'>Bienvenu à l'accueil</h1>
      {(token) ?
        <>
          <p className='title is-3'>Mes commandes</p>
          <table className="table">
            <thead>
              <tr>
                {keys.map(key => (
                  <th><abbr title={key}>{key}</abbr></th>
                ))}
                <td>Actions</td>
              </tr>
            </thead>
            <tbody>
              {commands.map(command => (
                <tr>
                  {keys.map(key => (
                    <td>{command[key]}</td>
                  ))}
                  <td className="is-flex is-flex-direction-column">
                    <Link to={`/command/${command['id']}`} className="button is-info my-1"><Eye /></Link>
                  </td>
                </tr>
              ))}

            </tbody>
          </table>
        </>
        :
        <></>}
    </div>
  );
}
export default HomeView;