import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {EcommerceContext} from "../context/ecommerce"

export function Logout() {
    const { setToken, setAdmin } = useContext(EcommerceContext);
    const navigate = useNavigate()

    useEffect(() => {
        setToken("")
        setAdmin(false)
        navigate('/')
    }, []);
}