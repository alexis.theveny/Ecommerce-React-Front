import '../App.css';
import React, { useContext, useEffect, useState } from 'react';
import { EcommerceContext } from "../context/ecommerce"
import { useNavigate } from 'react-router-dom';
import { createCommand } from '../api/command';
function CartView() {
    const { cartProducts, setCartProducts, token } = useContext(EcommerceContext);
    const [prixTotal, setPrixTotal] = useState(0)
    const navigate = useNavigate()

    useEffect(() => {
        console.log('use effect cart view');
        // calcul prix total
        let total = 0
        cartProducts.map( item => (
            total += item.quantity * item.product.prix
            ))
            setPrixTotal(total)
        }, [cartProducts])
        
    const submitForm = (e) => {
        console.log('submitForm cart view');
        e.preventDefault();
        let itemList = []
        cartProducts.map(item => (
            itemList = [...itemList, { id: item.product.id, qte: item.quantity }]
        ))
    
        createCommand(itemList, token).then(data => {
            navigate("/")
            setCartProducts([])
        });
        console.log(cartProducts)
    }
    const cancelCart = () => {
        setCartProducts([])
    }
    return (
        <div className='app'>
            {(cartProducts.length) ?
                <div className='cart'>
                    <ul>
                        <p className='title is-4' style={{textAlign:'center'}}>Détail du panier</p>
                        {cartProducts.map(item => (
                            <>
                                <li>
                                    <span className='title is-6'>{item.product.nom}</span>
                                    <br />
                                    <span className='is-italic'>{item.product.description}</span>
                                    <br />
                                    <span className='has-text-weight-bold'>{item.quantity}x{item.product.prix}</span>
                                </li>
                                <hr />
                            </>
                        ))}
                        <button className='button is-danger is-light' onClick={cancelCart}>Vider le panier</button>
                    </ul>
                    <form onSubmit={submitForm} className='box cart-box'>
                        <p className='title is-4'>Montant du panier</p>
                        <span className='title is-6'>Prix :</span>
                        <br/>
                        <p>{prixTotal}</p>
                        <br/>
                        <br/>{(token) ?
                            <button className="button is-primary is-light">Valider le panier</button>
                         : <p className="is-italic" style={{color:'red'}}>Veuillez vous connecter pour passer une commande</p>}
                    </form>
                </div>
                :
                <p className='title is-6' style={{textAlign:'center'}}>Le panier est vide</p>
            }

        </div>
    );
}
export default CartView;