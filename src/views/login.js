import '../App.css';
import React, { useContext, useState } from 'react';
import { connectUser } from '../api/user';
import {EcommerceContext} from "../context/ecommerce"
import { useNavigate } from 'react-router-dom';


function LoginView() {
    const [email, setEmail] = useState("julien@jdedev.fr")
    const [pass, setPass] = useState("test")
    const {setAdmin, setToken} = useContext(EcommerceContext)
    const navigate = useNavigate()


    const submitForm = (e) => {
        e.preventDefault();
        connectUser(email, pass).then(data => {
            if(typeof data.token !== 'string'){
                alert(data.mess)
            } else {
                setToken(data.token)
                setAdmin(data.admin === 1)
                navigate("/")
            }
        })
        console.log(email, pass)
    }
    return (
        <div className='app'>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Connexion</h1>
                    <input className="input" type="email" placeholder="Mail" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    <input className="input" type="password" placeholder="Password" value={pass} onChange={(e) => setPass(e.target.value)}/>
                    <button className="button is-primary is-light">Connexion</button>
                </form>
            </div>
        </div>
    );
}
export default LoginView;