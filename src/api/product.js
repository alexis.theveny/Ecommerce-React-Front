export function getAllProducts() {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/product", {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function getProduct(id) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/product/${id}`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}