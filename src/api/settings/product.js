export function addProduct(nom, description, prix, photo, token) {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/product", {
            method: "POST",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ nom, description, prix, photo })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function editProduct(id, nom, description, prix, photo, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/product/${id}`, {
            method: "PUT",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ nom, description, prix, photo })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function deleteProduct(id, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/product/${id}`, {
            method: "DELETE",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}