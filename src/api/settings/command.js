export function editCommand(id, statut, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/command/${id}`, {
            method: "PUT",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ statut })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function deleteCommand(id, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/command/${id}`, {
            method: "DELETE",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}