export function getAllUsers() {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/user`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function editUser(id, niveau, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/user/update/${id}`, {
            method: "PUT",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ niveau })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function deleteUser(id, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/user/delete/${id}`, {
            method: "DELETE",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}