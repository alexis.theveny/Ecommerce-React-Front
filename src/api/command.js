function createCommand(itemList, token) {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/command", {
            method: "POST",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ articles: itemList })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

function getAllCommands(token = null) {
    return new Promise((resolve, reject) => {
        if (token !== null) {
            fetch("http://localhost:3000/command", {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                    return resolve(data)
                })
                .catch(err => reject(err))
        } else {
            fetch("http://localhost:3000/command", {
                method: "GET"
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                    return resolve(data)
                })
                .catch(err => reject(err))

        }

    })
}
function getCommand(id) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/command/${id}`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
function getCommandLines(id) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/command/lines/${id}`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

export { createCommand, getAllCommands, getCommand, getCommandLines }