import React from 'react';
import { Link } from 'react-router-dom';
function Card({id, nom, description, photo, prix}) {
    return (
        <div className="card" >
            <div className="card-image">
                <figure className="image">
                    <img className="product-photo" src={photo} alt={photo} />
                </figure>
            </div>
            <div className="card-content">
                <div className="media">
                    <div className="media-content">
                        <p className="title is-4" title={nom} style={{color:'#00D1B2'}}>{nom}</p>
                        <p className="subtitle is-6 box price" style={{backgroundColor: '#00D1B2', fontWeight: 'bold'}}>{prix} €</p>
                    </div>
                </div>

                <div className="content">
                    {description}
                </div>
            </div>
            <footer className="card-footer">
                <Link to={`/product/${id}`} className="card-footer-item">
                    Voir
                </Link>
            </footer>
        </div>
    );
}
export default Card;