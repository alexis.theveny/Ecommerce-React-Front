import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { EcommerceContext } from '../context/ecommerce';
import { Cart } from 'react-bootstrap-icons'


function Header(props) {
    const { token, admin, cartProducts } = useContext(EcommerceContext);
    const [prixTotal, setPrixTotal] = useState(0)
    useEffect(() => {
        // calcul prix total
        let total = 0
        cartProducts.map( item => (
            total += item.quantity * item.product.prix
        ))
        setPrixTotal(total)
    }, [cartProducts])

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <h1 className="navbar-item title is-4">
                    E-Commerce React
                </h1>
            </div>

            <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                    <Link to="/" className="navbar-item">
                        Accueil
                    </Link>

                    <Link to="/product" className="navbar-item">
                        Produits
                    </Link>

                    {(admin === true) ?
                        <>
                            <div className="navbar-item has-dropdown is-hoverable">
                                <div className="navbar-link">
                                    Paramètres
                                </div>

                                <div className="navbar-dropdown">
                                    <Link to="/settings/product" className="navbar-item">
                                        Produits
                                    </Link>
                                    <Link to="/settings/user" className="navbar-item">
                                        Utilisateurs
                                    </Link>
                                    <hr className="navbar-divider" />
                                    <Link to="/settings/command" className="navbar-item">
                                        Commandes
                                    </Link>
                                </div>
                            </div>
                        </>
                        :
                        <>
                        </>
                    }
                </div>


                <div className="navbar-end">
                    <div className="navbar-item">
                        <Link to={'/cart'}>
                        <div className="control">
                            <div className="tags has-addons ">
                                <span className="tag is-dark is-large"><Cart /></span>
                                <span className="tag is-info is-large">{prixTotal} €</span>
                            </div>
                        </div>
                        </Link>
                    </div>
                    <div className="navbar-item">
                        <div className="buttons">
                            {(token !== "") ?
                                <>
                                    <Link to="/logout" className="button is-light is-danger">
                                        Déconnexion
                                    </Link>
                                </>
                                :
                                <>
                                    <Link to="/register" className="button is-primary">
                                        <strong>Inscription</strong>
                                    </Link>
                                    <Link to="/login" className="button is-light">
                                        Connexion
                                    </Link>
                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}
export default Header;