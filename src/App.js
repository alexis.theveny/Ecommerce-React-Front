import './App.css';
import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomeView from './views/home';
import Header from './components/header';
import { ProductView, ProductDetailView } from './views/product';
import LoginView from './views/login';
import RegisterView from './views/register';
import { ProductSettingView, ProductAddView, ProductEditView, ProductDeleteView } from './views/settings/product';
import {UserSettingView,  UserDeleteView, UserEditView } from './views/settings/user';
import {CommandSettingView, CommandEditView, CommandDeleteView} from './views/settings/command';
import { EcommerceContext } from './context/ecommerce';
import { Logout } from './views/logout';
import CartView from './views/cart';
import { CommandDetailView } from './views/command';

function App() {
  const [token, setToken] = useState("")
  const [admin, setAdmin] = useState(false)
  const [cartProducts, setCartProducts] = useState([])
  return (
    <EcommerceContext.Provider value={{ token, setToken, admin, setAdmin,cartProducts, setCartProducts }}>
      <Router>
        <Header></Header>
        <div className='app'>
          <Routes>
            <Route exact path='/' element={<HomeView />}></Route>
            <Route exact path='/product' element={< ProductView />}></Route>
            <Route exact path='/product/:id' element={< ProductDetailView />}></Route>
            <Route exact path='/command/:id' element={< CommandDetailView />}></Route>
            <Route exact path='/cart' element={< CartView />}></Route>
            <Route exact path='/settings/product' element={< ProductSettingView />}></Route>
            <Route exact path='/settings/product/add' element={< ProductAddView />}></Route>
            <Route exact path='/settings/product/edit/:id' element={< ProductEditView />}></Route>
            <Route exact path='/settings/product/delete/:id' element={< ProductDeleteView />}></Route>
            <Route exact path='/settings/user' element={< UserSettingView />}></Route>
            <Route exact path='/settings/user/edit/:id' element={< UserEditView />}></Route>
            <Route exact path='/settings/user/delete/:id' element={< UserDeleteView />}></Route>
            <Route exact path='/settings/command' element={< CommandSettingView />}></Route>
            <Route exact path='/settings/command/edit/:id' element={<  CommandEditView/>}></Route>
            <Route exact path='/settings/command/delete/:id' element={< CommandDeleteView />}></Route>
            <Route exact path='/login' element={< LoginView />}></Route>
            <Route exact path='/logout' element={< Logout />}></Route>
            <Route exact path='/register' element={< RegisterView />}></Route>
          </Routes>
        </div>
      </Router>
    </EcommerceContext.Provider>
  );
}
export default App;